package com.example.ruangdosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void logout(View view){
        FirebaseAuth.getInstance().signOut();
        finishAffinity();
        startActivity(new Intent(getApplicationContext(), Register.class));
    }

    public void ruangBelajar(View view){
        startActivity(new Intent(getApplicationContext(), RuangBelajar.class));
    }

    public void ruangLatihan(View view){
        startActivity(new Intent(getApplicationContext(), Ruanglatihan.class));
    }

    public void ruangDiskusi(View view) {
        startActivity(new Intent(getApplicationContext(), RuangDiskusi.class));
    }

    public void ruangInformasi(View view) {
        startActivity(new Intent(getApplicationContext(), RuangInformasi.class));
    }

}