package com.example.ruangdosen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class RuangDiskusi extends AppCompatActivity {

    private ListView listView;

    private String[] DaftarKontak = {
            "Dr.Eng. I Gde Putu Wirarama WW., ST.,",
            "Arik Aranta, S.Kom., M.Kom.",
            "Ahmad Zafrullah M., S.T., M.Eng.",
            "Gibran Satya Nugraha, S.Kom., M.Eng.",
            "Ramaditia Dwiyansaputra, S.T., M.Eng.",
            "Noor Alamsyah, ST,. MT.",
            "Prof. Dr. Eng. I Gede Pasek Suta Wijaya, ST., MT. ",
            "Fitri Bimantoro, ST., MT.",
            "Ariyan Zubaidi, S.Kom., MT",
            "Moh. ALi Albar, ST., M.Eng.",
            "Ir. Sri Endang Anjarwani, M.Kom",
            "Dr. Eng. Budi Irmawati S.Kom., MT.",
            "Ario Yudo Husodo, ST., MT.",
            "Andy Hidayat Jatmika, S.T., M.Kom",
    };

    private final String[] NomorHp = {
            "081804747497",
            "087865683836",
            "081804128129",
            "081804747455",
            "087765860861",
            "083129923144",
            "087765123832",
            "081623743523",
            "087865743212",
            "087765112432",
            "081804747497",
            "083129321422",
            "087865686123",
            "081232423524",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_kontak);

        listView = findViewById(R.id.list_view);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                DaftarKontak
        );

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                Toast.makeText(
                        RuangDiskusi.this,
                        "Nomor Hp : " + NomorHp[i],
                        Toast.LENGTH_LONG
                ).show();
                Log.d("RuangDiskusi", NomorHp[i]);
            }
        });
    }

    public void lihatKalori(View view) {
        String url = "https://samyunwan.com/tabel-daftar-kalori-makanan-dan-minuman/" ;
        Intent bukabrowser = new Intent(Intent. ACTION_VIEW);
        bukabrowser.setData(Uri. parse(url));
        startActivity(bukabrowser);
    }

}